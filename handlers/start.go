package handlers

import (
    "context"

    "github.com/volatiletech/sqlboiler/boil"
    tb "gitlab.com/bonch.dev/go-lib/telebot"
    "gitlab.com/bonch.dev/hacks/hackuniversity-2020/eagles/fridgefood_chief_bot/models"
)

func StartHandle(handler *Handler) func(m *tb.Message) {
    return (&startHandler{*handler}).Handle
}

type startHandler struct {
    Handler
}

func (h *startHandler) Handle(m *tb.Message) {
    var user *models.User

    user, err := h.getAuthUserById(m.Sender)
    if err != nil {
        var newUser models.User
        newUser.TelegramID = m.Sender.Recipient()
        err := newUser.Insert(context.Background(), h.db, boil.Infer())
        if err != nil {
            h.logger.Errorf("[new user] [create error]: %v", err)

            if _, err := h.bot.Send(m.Sender, databaseError); err != nil {
                h.logger.Errorf("[new user] [create error] [send error]: %v", err)
            }

            return
        }

        if err := newUser.Reload(context.Background(), h.db); err != nil {
            h.logger.Errorf("[new user] [reload error]: %v", err)

            if _, err := h.bot.Send(m.Sender, databaseError); err != nil {
                h.logger.Errorf("[new user] [reload error] [send error]: %v", err)
            }

            return
        }

        h.logger.Infof("[new user]: created, with ID: %v", newUser.ID)
        user = &newUser
    }

    user.TelegramAdditional = ""

    if err := h.setUserState(user, StartState); err != nil {
        if _, err := h.bot.Send(m.Sender, stucked); err != nil {
            h.logger.Errorf("[text] [user found] [send error]: %v", err)
        }
    }

    if _, err := h.bot.Send(m.Sender, mainMenu, &tb.ReplyMarkup{
        ReplyKeyboard: mainKeyboard,
    }); err != nil {
        h.logger.Errorf("[new user] [send error]: %v", err)
    }
}
