package handlers

import (
	"context"
	"encoding/json"

	"github.com/volatiletech/sqlboiler/boil"
	tb "gitlab.com/bonch.dev/go-lib/telebot"
	"gitlab.com/bonch.dev/hacks/hackuniversity-2020/eagles/fridgefood_chief_bot/models"
)

func AddReceiptHandle(handler *Handler) func(m *tb.Message) {
	return (&addReceiptsHandler{*handler}).Handle
}

func AddReceiptDoneHandle(handler *Handler) func(m *tb.Message) {
	return (&addReceiptsHandler{*handler}).HandleDone
}

type addReceiptsHandler struct {
	Handler
}

func (h *addReceiptsHandler) Handle(m *tb.Message) {
	var user *models.User
	user, err := h.getAuthUserById(m.Sender)
	if err != nil {
		if _, err := h.bot.Send(m.Sender, userNotFound); err != nil {
			h.logger.Errorf("[text] [user not found] [send error]: %v", err)
		}

		return
	}

	user.TelegramState = string(AddReceiptState)
	user.TelegramAdditional = ""

	if _, err := user.Update(context.Background(), h.db, boil.Infer()); err != nil {
		if _, err := h.bot.Send(m.Sender, databaseError); err != nil {
			h.logger.Errorf("[text] [user not found] [send error]: %v", err)
		}

		return
	}

	if _, err := h.bot.Send(
		m.Sender,
		receiptAddText,
		&tb.ReplyMarkup{
			ReplyKeyboard: [][]tb.ReplyButton{
				{
					Back,
				},
			},
			ResizeReplyKeyboard: true,
		}); err != nil {
		h.logger.Errorf("[text] [user not found] [send error]: %v", err)
	}
}

func (h *addReceiptsHandler) HandleText(m *tb.Message) {
	var user *models.User

	user, err := h.getAuthUserById(m.Sender)
	if err != nil {
		if _, err := h.bot.Send(m.Sender, userNotFound); err != nil {
			h.logger.Errorf("[text] [user not found] [send error]: %v", err)
		}

		return
	}

	var receiptAdditional struct {
		ReceiptId int `json:"receipt_id"`
	}

	var receipt *models.Receipt

	if user.TelegramAdditional != "" {
		if err := json.Unmarshal([]byte(user.TelegramAdditional), &receiptAdditional); err != nil {
			if _, err := h.bot.Send(m.Sender, stucked); err != nil {
				h.logger.Errorf("[text] [user not found] [send error]: %v", err)
			}

			return
		}

		receipt, err = models.FindReceipt(context.Background(), h.db, receiptAdditional.ReceiptId)
		if err != nil {
			if _, err := h.bot.Send(m.Sender, databaseError); err != nil {
				h.logger.Errorf("[text] [user not found] [send error]: %v", err)
			}

			return
		}
	} else {
		receipt = &models.Receipt{
			Description: m.Text,
			IsApproved:  false,
		}

		if err := user.AddReceipts(context.Background(), h.db, true, receipt); err != nil {
			if _, err := h.bot.Send(m.Sender, databaseError); err != nil {
				h.logger.Errorf("[text] [user not found] [send error]: %v", err)
			}

			return
		}

		if err := receipt.Reload(context.Background(), h.db); err != nil {
			if _, err := h.bot.Send(m.Sender, databaseError); err != nil {
				h.logger.Errorf("[text] [user not found] [send error]: %v", err)
			}

			return
		}

		receiptAdditional.ReceiptId = receipt.ID

		buffered, err := json.Marshal(receiptAdditional)
		if err != nil {
			if _, err := h.bot.Send(m.Sender, stucked); err != nil {
				h.logger.Errorf("[text] [user not found] [send error]: %v", err)
			}

			return
		}

		user.TelegramAdditional = string(buffered)

		if _, err := user.Update(context.Background(), h.db, boil.Infer()); err != nil {
			if _, err := h.bot.Send(m.Sender, databaseError); err != nil {
				h.logger.Errorf("[text] [user not found] [send error]: %v", err)
			}

			return
		}
	}

	if m.Photo != nil {
		if err := receipt.AddRecipeMedia(context.Background(), h.db, true, &models.Medium{
			MediaType: "photo",
			MediaURL:  m.Photo.FileID,
		}); err != nil {
			if _, err := h.bot.Send(m.Sender, databaseError); err != nil {
				h.logger.Errorf("[text] [user not found] [send error]: %v", err)
			}

			return
		}
	}

	if m.Video != nil {
		if err := receipt.AddRecipeMedia(context.Background(), h.db, true, &models.Medium{
			MediaType: "video",
			MediaURL:  m.Video.FileID,
		}); err != nil {
			if _, err := h.bot.Send(m.Sender, databaseError); err != nil {
				h.logger.Errorf("[text] [user not found] [send error]: %v", err)
			}

			return
		}
	}

	if _, err := h.bot.Send(
		m.Sender,
		receiptAddDone,
		&tb.ReplyMarkup{
			ReplyKeyboard:       [][]tb.ReplyButton{{Done}},
			ResizeReplyKeyboard: true,
		},
	); err != nil {
		h.logger.Errorf("[text] [user not found] [send error]: %v", err)
	}
}

func (h *addReceiptsHandler) HandleDone(m *tb.Message) {
	user, err := h.getAuthUserById(m.Sender)
	if err != nil {
		if _, err := h.bot.Send(m.Sender, userNotFound); err != nil {
			h.logger.Errorf("[text] [user not found] [send error]: %v", err)
		}

		return
	}

	user.TelegramAdditional = ""

	if err := h.setUserState(user, StartState); err != nil {
		if _, err := h.bot.Send(m.Sender, stucked); err != nil {
			h.logger.Errorf("[text] [user found] [send error]: %v", err)
		}
	}

	if _, err := h.bot.Send(
		m.Sender,
		receiptPhotoSaved,
		&tb.ReplyMarkup{
			ReplyKeyboard:       mainKeyboard,
			ResizeReplyKeyboard: true,
		},
	); err != nil {
		h.logger.Errorf("[text] [user not found] [send error]: %v", err)
	}
}
