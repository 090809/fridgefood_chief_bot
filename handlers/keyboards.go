package handlers

import tb "gitlab.com/bonch.dev/go-lib/telebot"

const addReceipt = "✏️ Добавить рецепт ✏️"

var AddReceipt = tb.ReplyButton{
    Text: addReceipt,
}

const myReceipts = "📜 Мои рецепты 📜"

var MyReceipts = tb.ReplyButton{
    Text: myReceipts,
}

const balance = "💰 Баланс 💰"

var Balance = tb.ReplyButton{
    Text: balance,
}

const done = "Готово"

var Done = tb.ReplyButton{
    Text: done,
}

const back = "Назад"

var Back = tb.ReplyButton{
    Text: back,
}

var InlineBack = tb.InlineButton{
    Unique: "back",
    Text:   back,
}

var mainKeyboard = [][]tb.ReplyButton{
    {
        AddReceipt,
    }, {
        MyReceipts,
    }, {
        Balance,
    },
}