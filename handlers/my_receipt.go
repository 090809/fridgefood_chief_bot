package handlers

import (
	"context"
	"fmt"

	tb "gitlab.com/bonch.dev/go-lib/telebot"
	"gitlab.com/bonch.dev/hacks/hackuniversity-2020/eagles/fridgefood_chief_bot/models"
)

func MyReceiptsHandle(handler *Handler) func(m *tb.Message) {
	return (&myReceiptsHandler{*handler}).Handle
}

type myReceiptsHandler struct {
	Handler
}

func (h *myReceiptsHandler) Handle(m *tb.Message) {
	var user *models.User

	user, err := h.getAuthUserById(m.Sender)
	if err != nil {
		if _, err := h.bot.Send(m.Sender, userNotFound); err != nil {
			h.logger.Errorf("[text] [user not found] [send error]: %v", err)
		}

		return
	}

	receipts, err := user.Receipts().All(context.Background(), h.db)
	if err != nil {
		if _, err := h.bot.Send(m.Sender, databaseError); err != nil {
			h.logger.Errorf("[text] [user not found] [send error]: %v", err)
		}

		h.logger.Error(err)

		return
	}

	var receiptList = myReceipts + "\n"
	var approvedList = "\n"
	var unApprovedList = "\n"

	for i, receipt := range receipts {
		if receipt.IsApproved {
			if len(receipt.Description) > 20 {
				approvedList += fmt.Sprintf("\n%d. ✅ %s...", i+1, (receipt.Description)[0:len(receipt.Description)-20])
			} else {
				approvedList += fmt.Sprintf("\n%d. ✅ %s", i+1, receipt.Description)
			}
		} else {
			if len(receipt.Description) > 20 {
				unApprovedList += fmt.Sprintf("\n%d. ⭕️ %s...", i+1, (receipt.Description)[0:len(receipt.Description)-20])
			} else {
				unApprovedList += fmt.Sprintf("\n%d. ⭕️ %s", i+1, receipt.Description)
			}
		}
	}

	approvedList += "\n"

	receiptList += "\nПроверенные и добавленные:" + approvedList + "Еще на проверке:\n" + unApprovedList

	_, err = h.bot.Send(m.Sender, receiptList)
	if err != nil {
		h.logger.Error(err)
	}
}
