package handlers

type TelegramState string

var (
    StartState TelegramState = "StartState"
    AddReceiptState TelegramState = "AddReceiptState"
)
